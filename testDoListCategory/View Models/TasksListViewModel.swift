//
//  TasksViewModel.swift
//  testDoListCategory
//
//  Created by Alex on 29.03.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation
import SwiftUI
import CoreData
import Combine

class TasksListViewModel: ObservableObject{
  //  var category:CategoryItem?
    var category:String?
   @Published var tasks = [TaskViewModel]()
  
    var coreDataManager : CoreDataManager
  
           
       
           
    
    
    
    init(category:String){
            coreDataManager = CoreDataManager(moc: NSManagedObjectContext.current)
        fetchAllTasksForCategory(category: category)
    }
    
    func fetchAllTasksForCategory(category:String){
        self.tasks = coreDataManager.getAllTaskForCategory(category: category).map(TaskViewModel.init)
        print(self.tasks)
    }
}


class TaskViewModel{
    var title = ""
    var done = false
    init(task:ToDoItem){
        self.title = task.title!
        self.done = task.done
    }
}


