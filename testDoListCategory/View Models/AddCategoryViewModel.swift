//
//  AddCategoryView.swift
//  testDoListCategory
//
//  Created by Alex on 29.03.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation
import SwiftUI
import CoreData
struct AddCategoryViewModel{
    
  
    var name:String = ""
    var coreDataManager : CoreDataManager
    init(){
    
       coreDataManager = CoreDataManager(moc: NSManagedObjectContext.current)
    
    }
    
    func saveCategory(){
        coreDataManager.saveNewCategoryToCoreData(name: name)
    }
    
}
