//
//  TasksViewModel.swift
//  testDoListCategory
//
//  Created by Alex on 29.03.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation
import SwiftUI
import CoreData
import Combine

class CategoryListViewModel: ObservableObject{
   
    
  
    @Published
    var categories = [CategoryViewModel]()
    
    var coreDataManager : CoreDataManager
    init(){
        
        coreDataManager = CoreDataManager(moc: NSManagedObjectContext.current)
        
        
       fetchAllCategories()
    }
    
    func fetchAllCategories(){
      //  self.categories = CoreDataManager.shared.getAllCategories().map(CategoryViewModel.init)
       
        self.categories = coreDataManager.getAllCategories().map(CategoryViewModel.init)
        print(self.categories)
    }
}


class CategoryViewModel{
    var name = ""
 
    init(category:CategoryItem){
        self.name = category.name!
     
    }
}


