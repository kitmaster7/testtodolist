//
//  AddTaskViewModel.swift
//  testDoListCategory
//
//  Created by Alex on 29.03.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation
import SwiftUI
import CoreData
struct AddTaskViewModel{
    
    var category:String?
    var title:String = " "
    var done:Bool = false
   
     
     var coreDataManager : CoreDataManager
    init(){
    
       coreDataManager = CoreDataManager(moc: NSManagedObjectContext.current)
    
    }
    
    
    func saveTaks(){
        coreDataManager.saveNewTaskToCoreData(name: self.title,category:category!)
    }
    
}
