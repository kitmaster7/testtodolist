//
//  CoreDataManager.swift
//  testDoListCategory
//
//  Created by Alex on 29.03.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager{
    
    
 //static let shared = CoreDataManager(moc: NSManagedObjectContext.current)
  
    
    
    
    
    var moc: NSManagedObjectContext
    
    init(moc:NSManagedObjectContext){
        self.moc = moc
    }
    
    
    func saveNewTaskToCoreData(name:String,category:String){
        
        
        let newItem = ToDoItem(context: self.moc)
        newItem.title = name
        newItem.done = false
        
        
        //find category by name
        
        let categoryPredicate = NSPredicate(format: "name == %@",category)
        let request: NSFetchRequest<CategoryItem> = CategoryItem.fetchRequest()
        
        
        request.sortDescriptors = []
        
        request.predicate = categoryPredicate
     
        
          var categories = [CategoryItem]()
                do {
                    categories =  try self.moc.fetch(request)
                } catch let error as NSError {
                    print(error)
                }
        
      
        
        
        print(categories.first?.name)
        
        newItem.parentCategory = categories.first
        
        saveCoreData()
        
    }
    
   func getAllCategories() -> [CategoryItem] {
    
     var categories = [CategoryItem]()
    
    let request: NSFetchRequest<CategoryItem> = CategoryItem.fetchRequest()
         
        
                        do {
                            categories =  try self.moc.fetch(request)
                        } catch let error as NSError {
                            print(error)
                        }
                
                print("GET CATEGORIES")
                print(categories)
                        return categories
                
      
         
         
       //  let categories =  getCategoriesFromCoreData(request: request)
    
    
 //   return categories
    }
    
    
    
    func getAllTaskForCategory(category:String) -> [ToDoItem] {
        
        //find category by name
        
        
        
        
        let categoryPredicate = NSPredicate(format: "parentCategory.name MATCHES %@",category)
        let request: NSFetchRequest<ToDoItem> = ToDoItem.fetchRequest()
        
        
        request.sortDescriptors = []
        
        request.predicate = categoryPredicate
        
        
        let tasks =  getTasksFromCoreData(request: request)
        
        
        
        
        
        return tasks
    }
    
    
    
    func saveNewCategoryToCoreData(name:String){
        
        
        let newCategory = CategoryItem(context: self.moc)
        newCategory.name = name
        
        
        saveCoreData()
        
    }
    
    func getCategoriesFromCoreData(request: NSFetchRequest<CategoryItem>) -> [CategoryItem] {
        
        var categories = [CategoryItem]()
                do {
                    categories =  try self.moc.fetch(request)
                } catch let error as NSError {
                    print(error)
                }
        
        print("GET CATEGORIES")
        print(categories)
                return categories
        
        
    }
    
    func getTasksFromCoreData(request: NSFetchRequest<ToDoItem>) -> [ToDoItem] {
          var tasks = [ToDoItem]()
          do {
              tasks =  try self.moc.fetch(request)
          } catch let error as NSError {
              print(error)
          }
          return tasks
      }
    
    
    
    private func saveCoreData() {
        if self.moc.hasChanges{
            
            do {
                try self.moc.save()
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    
    
    
    
    
}
