//
//  NSManagagedObject+Extension.swift
//  testDoListCategory
//
//  Created by Alex on 29.03.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import Foundation
import UIKit

import CoreData

extension NSManagedObjectContext{
    static var current: NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}
