//
//  AddTaskView.swift
//  testDoListCategory
//
//  Created by Alex on 19.02.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import SwiftUI
import CoreData
struct AddTaskView: View {

     @Environment(\.presentationMode) var presentationMode
   @State private var title = ""
    var selectedCategory: String = ""
   
    @State private var addTaskVM = AddTaskViewModel()
 
    init(category:String){
        self.selectedCategory = category
    }
    
    
   var body: some View {
       NavigationView{
           Form{
               Section{
          
               
                TextField("Имя задачи", text: self.$addTaskVM.title)
            
            }
               Section{
                   Button("Save"){
 
                    self.addTaskVM.done = false
                    self.addTaskVM.category = self.selectedCategory
                    
                    self.addTaskVM.saveTaks()
                  
                      self.presentationMode.wrappedValue.dismiss()
                   }
               }
           }.navigationBarTitle("Add task")
       }
   }
    
    
}

struct AddTaskView_Previews: PreviewProvider {
     static let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    static var previews: some View {
        let category = CategoryItem(context: moc)
               category.name = "test catergory"
        
        return NavigationView{
            AddTaskView(category: category.name!)
             }
       
    }
}
