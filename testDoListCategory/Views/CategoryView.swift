//
//  ContentView.swift
//  testDoListCategory
//
//  Created by Alex on 19.02.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import SwiftUI
import CoreData
struct CategoryView: View {
 
    
       @ObservedObject var categoriesListVM: CategoryListViewModel = CategoryListViewModel()
    
    
//    init(){
//        self.categoriesListVM
//    }
    
    
    @State private var showingAddCategoryScreen = false
    var body: some View {
        
        NavigationView{
            List{
                ForEach(self.categoriesListVM.categories, id: \.name){category in
                   NavigationLink(destination: TasksView(selectedCategory: category.name)){
                        
                      
                            Text(category.name)
                        
                    }
                    
                }
            }
         
                .navigationBarTitle("Список категорий")
                .navigationBarItems(trailing:
                    
                    Button(action:{
                    self.showingAddCategoryScreen.toggle()
                })
                    {
                    Image(systemName: "plus")
                    }
                    
            )
                .sheet(isPresented: $showingAddCategoryScreen, onDismiss: {
                    self.categoriesListVM.fetchAllCategories()
                }){
                 //   AddCategoryView().environment(\.managedObjectContext, self.moc)
                    
                  AddCategoryView()
                    
            }
        }.onAppear {
            print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
        }
        
        
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryView()
    }
}
