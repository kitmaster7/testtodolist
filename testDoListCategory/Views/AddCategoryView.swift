//
//  AddCategoryView.swift
//  testDoListCategory
//
//  Created by Alex on 19.02.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import SwiftUI

struct AddCategoryView: View {
  
      @State private var addCategoryVM = AddCategoryViewModel()
    
      @Environment(\.presentationMode) var presentationMode
    @State private var name = ""
    
    
    var body: some View {
        NavigationView{
            Form{
                Section{
                    TextField("Name of category", text: $name)
                }
                Section{
                    Button("Save"){
                   
                        self.addCategoryVM.name = self.name
                        self.addCategoryVM.saveCategory()
                        
                 
                        
                        self.presentationMode.wrappedValue.dismiss()
                    }
                }
            }.navigationBarTitle("Добавить категорию")
        }
    }
}

struct AddCategoryView_Previews: PreviewProvider {
    static var previews: some View {
        AddCategoryView()
    }
}
