//
//  TasksView.swift
//  testDoListCategory
//
//  Created by Alex on 19.02.2020.
//  Copyright © 2020 Alex. All rights reserved.
//

import SwiftUI
import CoreData
struct TasksView: View {
    
  let selectedCategory: String

    @ObservedObject var tasksListVM: TasksListViewModel
    
      @State private var showingAddTaskScreen = false
    
    
    
    init(selectedCategory:String){
        self.tasksListVM = TasksListViewModel(category: selectedCategory)
        
        
      self.selectedCategory = selectedCategory

    }
    
    
    var body: some View {
      
        
      
           
            List{
                ForEach(self.tasksListVM.tasks, id: \.title){todoItem in
             
                  
                        
                        VStack(alignment: .leading){
                            Text(todoItem.title )
                        }
                   
                    
                }
            }
            .navigationBarTitle(Text(selectedCategory ?? "UNKNOWN"))
                           .navigationBarItems(trailing:
                               
                               Button(action:{
                               self.showingAddTaskScreen.toggle()
                           })
                               {
                               Image(systemName: "plus")
                               }
                               
                       )
                .sheet(isPresented: $showingAddTaskScreen, onDismiss: {
                    self.tasksListVM.fetchAllTasksForCategory(category: self.selectedCategory)
                }){
                    AddTaskView(category:self.selectedCategory)
                       }
        
    }
          
}


struct TasksView_Previews: PreviewProvider {
   static let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    static var previews: some View {
       let category = CategoryItem(context: moc)
        category.name = "test catergory"
        return NavigationView{
           // TasksView(selectedCategory: category)
            TasksView(selectedCategory: category.name!)
        }
    }
}
